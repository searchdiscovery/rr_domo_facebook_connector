# DOMO Facebook Connector README #

This is a custom Domo connector which can be used to connect to the Facebook API. 

### High Level Details ###

* Currently, Domo's existing connector requires manually selecting which ad accounts you want to pull in metrics for, so this connector automates the process by pulling in specified metrics for all available accounts.
* This connector is for the DOMO IDE, and requires at least 30 days to get it Domo-approved.
* Depending on the Facebook account, there might be an additional 33 days before you can get Facebook's approval to access the full data without heavy API limits.
* The connector requires some setup and configuration before it can be used, both from Domo's side and from Facebook's side. Please see below for details.
* Version 1.3

### How do I get set up from Facebook's side? ###

* Set up an "app" in Facebook.Don't be confused by the word 'app'. It's simply a door to allow you to access the data. 
	1. Sign in to Facebook, and go to https://developers.facebook.com/apps. (If you do not have a Facebook Developers account, follow the prompts to enable that in your account). 
	2. Click the green "Add a New App" button
	3. Choose a name (Any name is good, just keep in mind that this name might be shown publicly later on.)
	4. Add two products: Facebook Login and Marketing API. (You'll set those up in the next step).

* Configure OAuth2.0 (both in your Facebook account.
	1. Find the "Facebook Login" product (It should be located in the side menu bar) and click "Quick Start". Then select Web.
	2. Enter https://developer.domo.com/builder/oauth as the site URL, and press Save. (You can ignore the Continue button).
	3. Click on the Settings option in the left menu. 
	3. Copy down the App ID and the App Secret. (This is your Client Key and Client Secret which you'll give Domo to enable OAuth).
	5. In the App Domains field, type in developer.domo.com, and press Save.
	6. Click on the Facebook Login option in the left menu
	7. In the field for "Valid OAuth redirect URIs", enter https://developer.domo.com/builder/oauth.php . Press Save.

* Later on, you'll want to submit your app for approval from Facebook.

### How do I get set up from Domo's side? ###

* Set up connector and configure user authentication
	1. Go to the Domo IDE (https://developer.domo.com/builder) and click to create a new connector. 
	2. Put in your Client Key (aka. App ID) and Client Secret (aka. App Secret).
	3. Authorization URL is: https://www.facebook.com/dialog/oauth 
	4. Access Token URL is: https://graph.facebook.com/oauth/access_token
	5. _Request method is POST
	6. Scope is email,user_posts,manage_pages,read_insights,ads_read,ads_management,business_management
	7. Paste in the authentication code (it's saved in the repository).
	8. Test it out. Click 'Get Access Token' and then 'Run Script'. The first time you run it, you should get a popup and then a 'success' message in Domo after you press okay.

* Add 'All Accounts' as a report.
* Add the parameter definitions (Enable Advanced Mode).
* Paste in the connector code and customize as needed.

### Reference Info ###

* Questions? Contact Shira Shkarofsky
* Facebook Marketing API documentation: https://developers.facebook.com/docs/marketing-api/insights
* Facebook Access Restrictions: https://developers.facebook.com/docs/marketing-api/access 
* Facebook API Limitations: https://developers.facebook.com/docs/marketing-api/api-rate-limiting