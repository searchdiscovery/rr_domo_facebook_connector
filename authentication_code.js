//Note: You will need to obtain your own client key and client secret.
//Please see https://developers.facebook.com/docs/facebook-login for more information


DOMO.log('metadata.account.code: ' + metadata.account.code);
DOMO.log('metadata.account.access_token: ' + metadata.account.accesstoken);

httprequest.addHeader('Authorization', 'Bearer ' +  metadata.account.accesstoken);

var res = httprequest.get('https://graph.facebook.com/v2.10/me');

DOMO.log('res: ' + res);

if(res.indexOf('id') > 0){auth.authenticationSuccess();}
else{
  auth.authenticationFailed('Your oauth token has expired.');
  
}